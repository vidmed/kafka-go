package main

import (
	"flag"
	"fmt"
)


var (
	configFileName string
	redirectorId   string
)

func init() {

	flag.StringVar(&redirectorId, "id", "redirector01", "Redirector ID")
	flag.StringVar(&configFileName, "config", "config.toml", "Config file name")
	flag.Parse()

	// init global vars
	Config := NewConfig(configFileName)
	GetLogger().Infof("Config loaded ", Config)

	GetStatisticsUpdater().SetId(redirectorId)
}

func main() {

	GetLogger().Infof("Start server ", redirectorId)
	defer GetStatisticsUpdater().Close()
	for i:=1; i<1001; i++ {
		GetStatisticsUpdater().Publish(fmt.Sprintf("message %v", i))
	}




//	defer GetStatisticsUpdater().Close()
//
//	// Trap SIGINT to trigger a shutdown.
//	signals := make(chan os.Signal, 1)
//	signal.Notify(signals, os.Interrupt)
//
//	var enqueued, errors int
//ProducerLoop:
//	for {
//		select {
//		case GetStatisticsUpdater().Producer.Input() <- &sarama.ProducerMessage{Topic: "repltest", Key: nil, Value: sarama.StringEncoder("testing 123")}:
//		//case GetStatisticsUpdater().Publish("message 1"):
//			enqueued++
//		case <-signals:
//			break ProducerLoop
//		}
//	}
//
//	log.Printf("Enqueued: %d; errors: %d\n", enqueued, errors)

}
