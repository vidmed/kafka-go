package main

import (
	"github.com/BurntSushi/toml"
)

var config_instance *TomlConfig

type TomlConfig struct {
	Title      string
	IsLocal    bool
	Main       MainConfig
	Maxmind    MaxmindConfig
	Wurfl      WurflConfig
	Statistics StatisticsConfig
}

type MainConfig struct {
	ApiUrl          string
	ApiAuthToken    string
	UpdatePeriodity int
	ListenStr       string
}
type MaxmindConfig struct {
	MaxmindCityDbPath       string
	MaxmindConnectionDbPath string
	MaxmindISPDbPath        string
}

type WurflConfig struct {
	WurflXmlPath string
}

type StatisticsConfig struct {
	NatsStreamingClusterId string
	NatsStreamingClientId  string
	NatsEventsSubject      string
	NatsURL                string

	KafkaAddrs             []string
	KafkaTopic             string
}

func GetConfig() *TomlConfig {
	return config_instance
}

func NewConfig(file string) *TomlConfig {
	config_instance = &TomlConfig{}

	if _, err := toml.DecodeFile(file, config_instance); err != nil {
		GetLogger().Error(err)
		return nil
	}
	return config_instance
}
