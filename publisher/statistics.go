package main

import (
	"sync"
	"github.com/Shopify/sarama"
)

var stat_once sync.Once
var stat_instance *Updater

type Updater struct {
	Producer sarama.AsyncProducer
	id string

}
func (u *Updater) Close(){
	u.Producer.Close()
}

func (u *Updater) SetId(id string){
	u.id = id
}


func (u *Updater) Publish(s string)  {
	GetLogger().Infof("Publish to ", GetConfig().Statistics.KafkaTopic)
	message := &sarama.ProducerMessage{Topic: GetConfig().Statistics.KafkaTopic, Value: sarama.StringEncoder(s)}
	u.Producer.Input() <- message
}

// TODO refactoring Configuration
func GetStatisticsUpdater() *Updater {
	stat_once.Do(func() {
		stat_instance = &Updater{}
		config := sarama.NewConfig()
		config.Producer.Partitioner = sarama.NewRoundRobinPartitioner
		var err error
		stat_instance.Producer, err = sarama.NewAsyncProducer(GetConfig().Statistics.KafkaAddrs, config)
		if err != nil {
			panic(err)
		}
	})
	return stat_instance
}


//--------------------------------------old (with nats streaming server)
//package main
//
//import (
//	"sync"
//	"github.com/nats-io/go-nats-streaming"
//)
//
//var stat_once sync.Once
//var stat_instance *Updater
//
//type Updater struct {
//	sc stan.Conn
//	id string
//
//}
//func (u *Updater) Close(){
//	u.sc.Close()
//}
//
//func (u *Updater) SetId(id string){
//	u.id = id
//}
//
//
//func (u *Updater) Publish(s string)  {
//
//	GetLogger().Infof("Publish to ", GetConfig().Statistics.NatsEventsSubject)
//	u.sc.PublishAsync(GetConfig().Statistics.NatsEventsSubject, []byte(s), func(ackedNuid string, err error){})
//}
//
//// TODO refactoring NATS connection
//func GetStatisticsUpdater() *Updater {
//	stat_once.Do(func() {
//		stat_instance = &Updater{}
//		stat_instance.sc, _ = stan.Connect(GetConfig().Statistics.NatsStreamingClusterId,
//			GetConfig().Statistics.NatsStreamingClientId,
//			stan.NatsURL(GetConfig().Statistics.NatsURL))
//	})
//	return stat_instance
//}
