package main

import (
	"github.com/Sirupsen/logrus"
	"sync"
)

var logger_once sync.Once
var logger_instance *logrus.Logger

func GetLogger() *logrus.Logger {

	logger_once.Do(func() {
		logger_instance = logrus.New()
		//logger_instance.Out = os.Stderr
		logger_instance.Level = logrus.DebugLevel
		//logger_instance.Level = logrus.WarnLevel

	})

	return logger_instance
}
